package com.johan.scenario2;

import com.johan.scenario2.impl.StatusAction;
import com.johan.scenario2.impl.StatusFilter;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
//        Sequential stream run on starting from the first iteration to last in said sequence
        viewAllNonEmptyUsingImpl(getStatuses().stream());
        viewAllNonEmptyUsingAnonClasses(getStatuses().stream());
        viewAllNonEmptyUsingLambda(getStatuses().stream());
//        Parallel stream allow the iterations to in parallel, thus parallel streams should not be used when iterations rely on each other
        viewAllNonEmptyUsingImpl(getStatuses().parallelStream());
        viewAllNonEmptyUsingAnonClasses(getStatuses().parallelStream());
        viewAllNonEmptyUsingLambda(getStatuses().parallelStream());
    }

    public static void viewAllNonEmptyUsingLambda(Stream<String> statusesStream) {
        statusesStream.filter(s -> !"".equals(s))
                .forEach(s -> System.out.println(s));
    }

    public static void viewAllNonEmptyUsingImpl(Stream<String> statusesStream) {
        statusesStream.filter(new StatusFilter())
                .forEach(new StatusAction());
    }

    public static void viewAllNonEmptyUsingAnonClasses(Stream<String> statusesStream) {
        statusesStream.filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return !"".equals(s);
            }
        })
                .forEach(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println(s);
                    }
                });
    }

    public static List<String> getStatuses() {
        List<String> statuses = Arrays.asList("Received", "Pending", "", "Awaiting Maturity",
                "Completed", "");
        return statuses;
    }
}

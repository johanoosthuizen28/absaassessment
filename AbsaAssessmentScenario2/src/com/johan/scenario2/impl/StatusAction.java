package com.johan.scenario2.impl;

import java.util.function.Consumer;

public class StatusAction implements Consumer<String> {
    @Override
    public void accept(String s) {
        System.out.println(s);
    }
}

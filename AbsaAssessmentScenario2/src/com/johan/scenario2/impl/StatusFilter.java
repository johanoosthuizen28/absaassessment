package com.johan.scenario2.impl;

import java.util.function.Predicate;

public class StatusFilter implements Predicate<String> {
    @Override
    public boolean test(String s) {
        return !"".equals(s);
    }
}

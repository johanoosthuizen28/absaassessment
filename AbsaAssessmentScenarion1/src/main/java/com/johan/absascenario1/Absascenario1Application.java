package com.johan.absascenario1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Absascenario1Application {

	public static void main(String[] args) {
		SpringApplication.run(Absascenario1Application.class, args);
	}

}

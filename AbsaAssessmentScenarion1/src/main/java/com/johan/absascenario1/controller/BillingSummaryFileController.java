package com.johan.absascenario1.controller;

import com.johan.absascenario1.service.Impl.BillingSummaryFileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/billingsummaryfile")
public class BillingSummaryFileController {
    public final BillingSummaryFileServiceImpl billingFileSummaryService;

    @Autowired
    public BillingSummaryFileController(BillingSummaryFileServiceImpl billingFileSummaryService) {
        this.billingFileSummaryService = billingFileSummaryService;
    }

    @GetMapping("/files/{filename}")
    public byte[] getBillingFileSummary(@PathVariable(value = "filename") String fileName){
        return billingFileSummaryService.findBillingSummaryFile(fileName);
    }
}

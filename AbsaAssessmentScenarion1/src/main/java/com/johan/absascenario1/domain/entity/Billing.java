package com.johan.absascenario1.domain.entity;

import com.johan.absascenario1.domain.base.BaseEntity;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
public class Billing extends BaseEntity {

    private String transactionReference;

    private String clientSwiftAddress;

    private String messageStatus;

    private String currency;

    private int amount;

    private Date dateTimeCreated;

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }
}

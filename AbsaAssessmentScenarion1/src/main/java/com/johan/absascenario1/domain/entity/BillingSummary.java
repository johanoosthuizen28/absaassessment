package com.johan.absascenario1.domain.entity;

import com.johan.absascenario1.domain.base.BaseEntity;

import javax.persistence.Entity;
import java.sql.Date;

@Entity
public class BillingSummary extends BaseEntity {
    private String serviceName;

    private String clientSwiftAddress;

    private String subService;

    private Date date;

    private int usageStats;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public String getSubService() {
        return subService;
    }

    public void setSubService(String subService) {
        this.subService = subService;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUsageStats() {
        return usageStats;
    }

    public void setUsageStats(int usageStats) {
        this.usageStats = usageStats;
    }
}

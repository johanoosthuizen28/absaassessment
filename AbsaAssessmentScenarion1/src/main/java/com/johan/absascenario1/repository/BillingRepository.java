package com.johan.absascenario1.repository;

import com.johan.absascenario1.domain.entity.Billing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface BillingRepository extends JpaRepository<Billing, Long> {

    List<Billing> findAllByDateTimeCreatedGreaterThanEqualAndDateTimeCreatedLessThanEqualAndMessageStatusEquals(Date startDate, Date endDate, String messageStatus);

}
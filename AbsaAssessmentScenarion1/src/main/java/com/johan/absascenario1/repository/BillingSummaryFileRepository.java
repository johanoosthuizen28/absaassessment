package com.johan.absascenario1.repository;

import com.johan.absascenario1.domain.entity.BillingSummary;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Repository
public class BillingSummaryFileRepository {

    public void constructBillingSummaryFile(List<BillingSummary> billingSummaries) {
        String fileContents = getFileContents(billingSummaries);
        File file = new File("billingsummary" + LocalDate.now() + ".txt");
        FileWriter fileWriter = null;
        try {
            file.createNewFile();
            fileWriter = new FileWriter(file);
            fileWriter.write(fileContents);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public File findBillingSummaryFile(String fileName) {
        return new File(fileName);
    }

    private String getFileContents(List<BillingSummary> billingSummaries) {
        String fileContents = "";
        for (BillingSummary billingSummary : billingSummaries) {
            fileContents += String.format("%18s", billingSummary.getServiceName());
            fileContents += String.format("%12s", billingSummary.getClientSwiftAddress());
            fileContents += String.format("%7s", billingSummary.getSubService());
            fileContents += String.format("%9s", billingSummary.getDate());
            fileContents += String.format("%6s", billingSummary.getUsageStats() + "\n");
        }
        return fileContents;
    }
}

package com.johan.absascenario1.repository;

import com.johan.absascenario1.domain.entity.BillingSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingSummaryRepository extends JpaRepository<BillingSummary, Long> {

}

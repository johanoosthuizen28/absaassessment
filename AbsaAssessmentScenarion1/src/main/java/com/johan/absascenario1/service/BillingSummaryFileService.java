package com.johan.absascenario1.service;

public interface BillingSummaryFileService {
    byte[] findBillingSummaryFile(String fileName);
}

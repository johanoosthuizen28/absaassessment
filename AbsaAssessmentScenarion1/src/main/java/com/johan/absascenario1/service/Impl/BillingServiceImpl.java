package com.johan.absascenario1.service.Impl;

import com.johan.absascenario1.domain.entity.Billing;
import com.johan.absascenario1.domain.entity.BillingSummary;
import com.johan.absascenario1.repository.BillingRepository;
import com.johan.absascenario1.repository.BillingSummaryFileRepository;
import com.johan.absascenario1.repository.BillingSummaryRepository;
import com.johan.absascenario1.service.BillingService;
import com.johan.absascenario1.util.BillingSummaryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BillingServiceImpl implements BillingService {

    public static final String ZAR = "ZAR";
    private final BillingRepository billingRepository;
    private final BillingSummaryFileRepository billingSummaryFileRepository;
    private final BillingSummaryRepository billingSummaryRepository;

    @Autowired
    public BillingServiceImpl(BillingRepository billingRepository, BillingSummaryFileRepository billingSummaryFileRepository, BillingSummaryRepository billingSummaryRepository) {
        this.billingRepository = billingRepository;
        this.billingSummaryFileRepository = billingSummaryFileRepository;
        this.billingSummaryRepository = billingSummaryRepository;
    }

    @Override
    @Scheduled(cron = "0 0 7 1 * MON-FRI")
    public void billingSummaryScheduledTask() {
        List<BillingSummary> billingSummaries = generateBillingSummaryList();
        billingSummaryRepository.saveAll(billingSummaries);
        billingSummaryFileRepository.constructBillingSummaryFile(billingSummaries);
    }

    private Map<String, List<Billing>> generateClientSwiftAddressMap() {
        Date startDate = getStartDate();
        Date endDate = getEndDate();
        List<Billing> billings = billingRepository.findAllByDateTimeCreatedGreaterThanEqualAndDateTimeCreatedLessThanEqualAndMessageStatusEquals(startDate, endDate, "SUCCESSFUL");
        Map<String, List<Billing>> billingMap = new HashMap<>();
        for (Billing billing : billings) {
            if (!billingMap.containsKey(billing.getClientSwiftAddress() + "Z") && ZAR.equals(billing.getCurrency())) {
                List<Billing> tempList = billings.stream()
                        .filter(b -> billing.getClientSwiftAddress().equals(b.getClientSwiftAddress()))
                        .filter(b -> "ZAR".equals(b.getCurrency()))
                        .collect(Collectors.toList());
                billingMap.put(billing.getClientSwiftAddress() + "Z", tempList);
            } else if (!billingMap.containsKey(billing.getClientSwiftAddress() + "C") && !ZAR.equals(billing.getCurrency())) {
                List<Billing> tempList = billings.stream()
                        .filter(b -> billing.getClientSwiftAddress().equals(b.getClientSwiftAddress()))
                        .filter(b -> !"ZAR".equals(b.getCurrency()))
                        .collect(Collectors.toList());
                billingMap.put(billing.getClientSwiftAddress() + "C", tempList);
            }
        }
        return billingMap;
    }

    private List<BillingSummary> generateBillingSummaryList() {
        List<BillingSummary> billingSummaries = generateClientSwiftAddressMap().values().stream()
                .map(new BillingSummaryMapper()).collect(Collectors.toList());
        return billingSummaries;
    }

    private Date getEndDate() {
        LocalDate localDate2 = LocalDate.now();
        localDate2.minusMonths(1);
        localDate2.withDayOfMonth(localDate2.lengthOfMonth());
        return Date.valueOf(localDate2);
    }

    private Date getStartDate() {
        LocalDate localDate1 = LocalDate.now();
        localDate1.minusDays(1);
        localDate1.withDayOfMonth(1);
        return Date.valueOf(localDate1);
    }
}
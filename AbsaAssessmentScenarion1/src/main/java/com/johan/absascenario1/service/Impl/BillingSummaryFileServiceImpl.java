package com.johan.absascenario1.service.Impl;

import com.johan.absascenario1.repository.BillingSummaryFileRepository;
import com.johan.absascenario1.service.BillingSummaryFileService;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Service
public class BillingSummaryFileServiceImpl implements BillingSummaryFileService {
    private final BillingSummaryFileRepository billingSummaryFileRepository;

    public BillingSummaryFileServiceImpl(BillingSummaryFileRepository billingSummaryFileRepository) {
        this.billingSummaryFileRepository = billingSummaryFileRepository;
    }

    @Override
    public byte[] findBillingSummaryFile(String fileName) {
        File billingSummaryFile = billingSummaryFileRepository.findBillingSummaryFile(fileName);
        InputStream billingFileInputStream = null;
        try {
            billingFileInputStream = new FileInputStream(billingSummaryFile);
            return IOUtils.toByteArray(billingFileInputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

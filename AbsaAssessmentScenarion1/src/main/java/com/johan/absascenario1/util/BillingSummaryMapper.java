package com.johan.absascenario1.util;

import com.johan.absascenario1.domain.entity.Billing;
import com.johan.absascenario1.domain.entity.BillingSummary;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

public class BillingSummaryMapper implements Function<List<Billing>, BillingSummary> {

    public static final String INTEGRATEDSERVICES = "INTEGRATEDSERVICES";
    public static final String ZAROUT = "ZAROUT";
    public static final String CCYOUT = "CCYOUT";
    public final Date date = Date.valueOf(LocalDate.now());

    @Override
    public BillingSummary apply(List<Billing> billings) {
        BillingSummary billingSummary = new BillingSummary();
        billingSummary.setServiceName(INTEGRATEDSERVICES);
        billingSummary.setClientSwiftAddress(billings.get(0).getClientSwiftAddress());
        billingSummary.setUsageStats(billings.size());
        billingSummary.setDate(date);
        if ("ZAR".equals(billings.get(0).getCurrency())) {
            billingSummary.setSubService(ZAROUT);
        } else {
            billingSummary.setSubService(CCYOUT);
        }
        return billingSummary;
    }
}
